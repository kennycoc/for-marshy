const express = require('express');
const app = express();
const handlebars = require('express-handlebars')
const bodyParser = require('body-parser');
const fs = require('fs');

const port = process.env.PORT || 5000;

app.use(bodyParser.urlencoded({
  extended: true
}));
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');

// Define root route.
app.get('/', (req, res) => {
    fs.readFile('/tmp/num', (err, data) => {
        var num = data.toString();
        res.render('home', {num: num});
    });
});

app.post('/', (req, res) => {
    num = parseInt(req.body.num) + 1;
    fs.writeFile('/tmp/num', num, () => {/* don't care about errors */});
    return res.redirect('/');
});

app.listen(port);
